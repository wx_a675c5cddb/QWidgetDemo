/********************************************************************************
** Form generated from reading UI file 'frmhotkey.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRMHOTKEY_H
#define UI_FRMHOTKEY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_frmHotKey
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;

    void setupUi(QWidget *frmHotKey)
    {
        if (frmHotKey->objectName().isEmpty())
            frmHotKey->setObjectName(QStringLiteral("frmHotKey"));
        frmHotKey->resize(400, 300);
        verticalLayout = new QVBoxLayout(frmHotKey);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(frmHotKey);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);


        retranslateUi(frmHotKey);

        QMetaObject::connectSlotsByName(frmHotKey);
    } // setupUi

    void retranslateUi(QWidget *frmHotKey)
    {
        frmHotKey->setWindowTitle(QApplication::translate("frmHotKey", "\345\205\250\345\261\200\347\203\255\351\224\256\347\244\272\344\276\213", 0));
        label->setText(QApplication::translate("frmHotKey", "\346\214\211 ctrl+x \346\234\200\345\260\217\345\214\226,\345\206\215\346\254\241\346\214\211\346\230\276\347\244\272", 0));
    } // retranslateUi

};

namespace Ui {
    class frmHotKey: public Ui_frmHotKey {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRMHOTKEY_H
